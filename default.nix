{ pkgs ? import <nixpkgs> {}}:

pkgs.stdenv.mkDerivation {
  name = "vpn-notifier";

  src = ./.;

  buildInputs = with pkgs; [
    vala
    pkg-config
    libnotify
  ];

  buildPhase = ''
    valac --pkg gio-2.0 --pkg libnotify vpn-notifier.vala
    valac --pkg gio-2.0 vpn-listener.vala
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp vpn-notifier $out/bin/vpn-notifier
    cp vpn-listener $out/bin/vpn-listener
  '';
}
