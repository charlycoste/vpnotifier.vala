[DBus (name = "dev.coste.desktop.vpn")]
interface VPNState : Object {
    public signal void on ();
    public signal void off ();
}

int main () {

    Notify.init ("VPN notifications");

    VPNState vpn;

    try {
        // We instanciate a proxy to listen for the DBus object
        vpn = Bus.get_proxy_sync (BusType.SESSION, "dev.coste.desktop.vpn", "/dev/coste/desktop/vpn");
    } catch (Error e) {
        error ("Error: %s", e.message);
    }

    vpn.on.connect (() => {
        try {
            new Notify.Notification ("VPN", "VPN connected", "dialog-information").show ();
        } catch (Error e) {
            error ("Error: %s", e.message);
        }
    });

    vpn.off.connect (() => {
        try {
            new Notify.Notification ("VPN", "VPN disconnected", "dialog-warning").show ();
        } catch (Error e) {
            error ("Error: %s", e.message);
        }
    });

    new MainLoop ().run ();

    return 0;
}
