VPNotifier.vala
===============

![Screenshot of desktop notification](screenshot.png)

What is VPNotifier.vala ?
-------------------------

VPNotifier.vala is a set of programs (written in Vala language, of course) to monitor VPN connections.

Introduction
------------

One of my customers needed to use a VPN client called "Pulse Secure" which is obviously neither Free
nor Open Source, and particularly not well suited for GNU/Linux. A common problem of this client is
that sometimes it can be disconnected but does’nt warn you about it.
So I tried to write a little DBus program that can be informed when a disconnection happens and notify me.
To make it more extendable, I separated this into two different programs :

- The listener which listens to Network Manager events and sends a signal when the VPN state is changing
- The notifier which sends a notification to my gnome Desktop when the listener sends a signal

How to use it ?
---------------

If you are using Gnome desktop you may only need the vala compiler and development libraries for libnotify.

For example, on Ubuntu : `sudo apt install valac libnotify-dev`

Then, you can compile the two programs this way :

```sh
valac --pkg gio-2.0 --pkg libnotify vpn-notifier.vala
valac --pkg gio-2.0 vpn-listener.vala
```

Then, you can launch the programs :

```sh
./vpn-listener &
./vpn-notifier &
```

(the `&` character allows you to launch the programs in background. But remember that if you close your terminal,
those processes will be killed. So if you want to launch them as a service, you need to use your service manager)

License
-------

[![GPL](https://www.gnu.org/graphics/gplv3-127x51.png)](https://www.gnu.org/licenses/gpl-3.0.en.html)

Copyright (C) 2021  Charles-Édouard Coste

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
