
// This DBus interface allows us to listen to events from network manager
// (We don’t need every signals, only device_added and device_removed)
[DBus (name = "org.freedesktop.NetworkManager")]
interface NetworkManager : Object {
    // @see https://hev.cc/2372.html
    // @see https://wiki.gnome.org/Projects/Vala/DBusServerSample
    public signal void device_added (GLib.ObjectPath path);
    public signal void device_removed (GLib.ObjectPath path);
}

// This DBus interface allows us to get informations about the driver
// used by a device. If the driver is "tun", then it’s a VPN
[DBus (name = "org.freedesktop.NetworkManager.Device")]
interface Device : Object {
    [DBus (name = "Driver")]
    public abstract string driver { owned get; }
}

// Our business DBus interface
[DBus (name = "dev.coste.desktop.vpn")]
class VPNState : Object {
    public signal void on();  // when VPN is connected
    public signal void off(); // when VPN is disconnected
    public string path;       // current ObjectPath used for the VPN (for instance: "/org/freedesktop/NetworkManager/Device/19")
}

int main () {
    VPNState state = new VPNState();
    try {
        Bus.own_name (
            BusType.SESSION,
            "dev.coste.desktop.vpn",
            BusNameOwnerFlags.NONE,
            (conn) => {
                try {

                    conn.register_object("/dev/coste/desktop/vpn", state);

                } catch (GLib.Error e) {
                    stderr.printf ("DBus object creation for VPN notifications has failed : %s\n", e.message);
                }
            },
            () => {},
            () => stderr.printf ("Could not acquire name\n")
        );

        // We instanciate a proxy object to listen to DBus object
        NetworkManager network = Bus.get_proxy_sync (
            BusType.SYSTEM,
            "org.freedesktop.NetworkManager",
            "/org/freedesktop/NetworkManager"
        );

        network.device_added.connect((path) => {
            try {
                Device device = Bus.get_proxy_sync (
                    BusType.SYSTEM,
                    "org.freedesktop.NetworkManager",
                    path
                );

                if(device.driver == "tun") {
                    state.path = path;
                    state.on();
                }

            } catch (GLib.Error e) {
                stderr.printf ("Error while querying device driver : %s\n", e.message);
            }
        });

        network.device_removed.connect((path) => {
            if (state.path == path) {
                state.path = "";
                state.off();
            }
        });

        var loop = new MainLoop ();
        loop.run ();

    } catch (GLib.Error e) {
        stderr.printf ("%s\n", e.message);
        return 1;
    }

    return 0;
}
